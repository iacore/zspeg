# zspeg

Parser Combinator for Zig

This library was extracted from https://github.com/CurtisFenner/zsmol/.

I tried porting https://github.com/spadix0/cZPeg too, but the compiler kept crashing.

This library leaks memory. Be sure to only use with buffer/arena allocator.

